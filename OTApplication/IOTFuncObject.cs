﻿using System;
using System.Runtime.InteropServices;


namespace OTApplication
{
    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("A958B6F4-7C0E-4601-A59C-4C30A84F681F")]
    public interface IOTFuncObject
    {
        string AddDocument(string documentId);

        string AddObject(string objectId);
    }
}
