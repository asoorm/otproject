﻿using System;
using System.Runtime.InteropServices;

namespace OTApplication
{

    [ComVisible(true)]
    [Guid("EA47E4AB-B9B2-4E53-90EA-C84570756BA2")]
    public class OTFuncObject : IOTFuncObject
    {
        public string AddDocument(string documentId)
        {
            return $"Document Id is {documentId}";
        }

        public string AddObject(string objectId)
        {
            return $"Object Id is {objectId}";
        }
    }
}
